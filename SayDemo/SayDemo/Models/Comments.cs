﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SayDemo.Models
{
    public class Comments:BaseModel
    {
        public int MsgId { get; set; }
        public int FromUserId { get; set; }
        public string Content { get; set; }
    }
}