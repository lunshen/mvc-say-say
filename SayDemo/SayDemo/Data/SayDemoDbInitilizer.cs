﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SayDemo.Models;

namespace SayDemo.Data
{
    public class SayDemoDbInitilizer:DropCreateDatabaseAlways<SayDemoDb>
    {
        protected override void Seed(SayDemoDb db)
        {
            db.Users.Add(new Models.Users
            {
                Username = "admin",
                Password = "113",
                CreatedAt=DateTime.Now,
                UpdatedAt=DateTime.Now,
                Version=0
            });

            db.Users.Add(new Models.Users
            {
                Username = "太简单",
                Password = "113",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Version = 0
            });


            db.Users.Add(new Models.Users
            {
                Username = "太复杂",
                Password = "113",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Version = 0
            });


            db.Users.Add(new Models.Users
            {
                Username = "局部气候调查局",
                Password = "113",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Version = 0
            });


            db.Messages.AddRange(new List<Messages>
            {
                new Messages
                {
                    FromUserId=1,
                    Content="你的柔情我永远不懂",
                    CreatedAt=DateTime.Now,
                    UpdatedAt=DateTime.Now,
                    Version=0
                },
                new Messages
                {
                    FromUserId=1,
                    Content="大上海",
                    CreatedAt=DateTime.Now,
                    UpdatedAt=DateTime.Now,
                    Version=0
                },
                new Messages
                {
                    FromUserId=2,
                    Content="蜗牛和黄鹂鸟",
                    CreatedAt=DateTime.Now,
                    UpdatedAt=DateTime.Now,
                    Version=0
                },
            });

            db.Comments.AddRange(new List<Comments>
            {
                new Comments
                {
                    MsgId=1,
                    FromUserId=1,
                    Content="芳草依依",
                    CreatedAt=DateTime.Now,
                    UpdatedAt=DateTime.Now,
                    Version=0
                },
                new Comments
                {
                    FromUserId=1,
                    MsgId=1,
                    Content="最亲的人",
                    CreatedAt=DateTime.Now,
                    UpdatedAt=DateTime.Now,
                    Version=0
                },
                new Comments
                {
                    FromUserId=2,
                    MsgId=2,
                    Content="K娃",
                    CreatedAt=DateTime.Now,
                    UpdatedAt=DateTime.Now,
                    Version=0
                },
                new Comments
                {
                    FromUserId=1,
                    MsgId=2,
                    Content="流浪的人",
                    CreatedAt=DateTime.Now,
                    UpdatedAt=DateTime.Now,
                    Version=0
                },
            });

            db.SaveChanges();



            base.Seed(db);
        }
    }
}