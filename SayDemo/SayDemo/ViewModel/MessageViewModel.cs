﻿using SayDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SayDemo.ViewModel
{
    public class MessageViewModel
    {
        public int Id { get; set; }
        public int FromUserId { get; set; }
        public string Content { get; set; }
        public IEnumerable<CommentViewModel> Comments { get; set; } 
    }
}


